class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.string :description
      t.integer :number
      t.string :status
      t.references :environment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
