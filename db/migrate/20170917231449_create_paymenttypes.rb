class CreatePaymenttypes < ActiveRecord::Migration
  def change
    create_table :paymenttypes do |t|
      t.string :type
      t.integer :numberplots
      t.boolean :status
      t.date :date

      t.timestamps null: false
    end
  end
end
