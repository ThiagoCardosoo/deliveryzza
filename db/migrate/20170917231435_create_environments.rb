class CreateEnvironments < ActiveRecord::Migration
  def change
    create_table :environments do |t|
      t.string :description
      t.integer :numberboard

      t.timestamps null: false
    end
  end
end
