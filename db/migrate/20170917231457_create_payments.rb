class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :numberrequest
      t.integer :discount
      t.integer :numbernote
      t.float :amount
      t.references :paymenttype, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
