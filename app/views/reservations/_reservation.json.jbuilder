json.extract! reservation, :id, :description, :date, :hour, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
