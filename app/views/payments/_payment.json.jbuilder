json.extract! payment, :id, :numberrequest, :discount, :numbernote, :amount, :paymenttype_id, :created_at, :updated_at
json.url payment_url(payment, format: :json)
