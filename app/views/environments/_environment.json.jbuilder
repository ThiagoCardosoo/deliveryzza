json.extract! environment, :id, :description, :numberboard, :created_at, :updated_at
json.url environment_url(environment, format: :json)
