json.extract! paymenttype, :id, :type, :numberplots, :status, :date, :created_at, :updated_at
json.url paymenttype_url(paymenttype, format: :json)
